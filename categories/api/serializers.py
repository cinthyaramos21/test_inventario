from rest_framework import serializers
from django.utils.text import slugify

from categories.models import Category


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name', 'description']

    def create(self, validate_data):
        name = validate_data['name']
        instance = self.Meta.model(**validate_data)

        code = slugify(str(name))
        code = '-'.join([x.capitalize() for x in code.split('-')])

        instance.code = code.lower()

        instance.save()
        return instance

    def update(self, instance, validate_data):
        name = validate_data['name']

        code = slugify(str(name))
        code = '-'.join([x.capitalize() for x in code.split('-')])

        instance.code = code.lower()
        instance.name = validate_data.get('name', instance.name)
        instance.description = validate_data.get('description', instance.description)

        instance.save()
        return instance


class CategoryGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
