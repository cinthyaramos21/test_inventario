from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend

from categories.models import Category
from categories.api.serializers import CategorySerializer, CategoryGetSerializer
from categories.api.permissions import IsLoginOrReadOnly


class CategoryApiViewSet(ModelViewSet):
    permission_classes = [IsLoginOrReadOnly]
    queryset = Category.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name']

    def get_serializer_class(self):
        if self.action == 'list':
            return CategoryGetSerializer
        else:
            return CategorySerializer

