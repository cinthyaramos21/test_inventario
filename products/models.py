from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=50, unique=True)
    introduction_date = models.DateField(auto_now_add=True)
    sales_discontinuation_date = models.DateField()
    comment = models.TextField(blank=True)

    def __str__(self):
        return self.name
