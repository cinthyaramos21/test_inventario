from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend

from products.models import Product
from products.api.serializers import ProductSerializer, ProductGetSerializer
from products.api.permissions import IsLoginOrReadOnly


class ProductApiViewSet(ModelViewSet):
    permission_classes = [IsLoginOrReadOnly]
    queryset = Product.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'sales_discontinuation_date']

    def get_serializer_class(self):
        if self.action == 'list':
            return ProductGetSerializer
        else:
            return ProductSerializer

