from rest_framework import serializers

from classifications.models import Classification
from products.api.serializers import ProductGetSerializer
from categories.api.serializers import CategoryGetSerializer


class ClassificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Classification
        fields = ['from_date', 'thru_date', 'comment', 'product', 'category']


class ClassificationGetSerializer(serializers.ModelSerializer):
    product_data = ProductGetSerializer(source="product", read_only=True)
    category_data = CategoryGetSerializer(source="category", read_only=True)

    class Meta:
        model = Classification
        fields = '__all__'
