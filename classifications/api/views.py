from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend

from classifications.models import Classification
from classifications.api.serializers import ClassificationGetSerializer, ClassificationSerializer
from classifications.api.permissions import IsLoginOrReadOnly


class ClassificationApiViewSet(ModelViewSet):
    permission_classes = [IsLoginOrReadOnly]
    queryset = Classification.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['category__code', 'product__name', 'primary_flag']

    def get_serializer_class(self):
        if self.action == 'list':
            return ClassificationGetSerializer
        else:
            return ClassificationSerializer

