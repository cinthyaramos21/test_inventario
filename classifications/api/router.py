from rest_framework.routers import DefaultRouter
from classifications.api.views import ClassificationApiViewSet


router_classification = DefaultRouter()

router_classification.register(prefix='classifications', basename='classifications', viewset=ClassificationApiViewSet)