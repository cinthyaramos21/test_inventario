from django.contrib import admin
from classifications.models import Classification


@admin.register(Classification)
class ClassificationAdmin(admin.ModelAdmin):
    list_display = ['from_date', 'thru_date', 'comment']