from django.db import models
from django.db.models import CASCADE

from categories.models import Category
from products.models import Product


class Classification(models.Model):
    from_date = models.DateField()
    thru_date = models.DateField()
    primary_flag = models.BooleanField(default=False)
    comment = models.TextField(blank=True)
    product = models.ForeignKey(Product, on_delete=CASCADE, blank=False)
    category = models.ForeignKey(Category, on_delete=CASCADE, blank=False)

